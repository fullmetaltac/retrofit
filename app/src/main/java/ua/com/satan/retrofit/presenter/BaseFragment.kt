package ua.com.satan.retrofit.presenter

import android.os.Bundle
import android.support.v4.app.Fragment

abstract class BaseFragment<in V : BaseView, T : BasePresenter<V>> : Fragment(), BaseView {

    lateinit var presenter: T

    abstract fun providePresenter(): T

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        presenter = providePresenter()
        initViews()
    }

    open fun initViews() {
        // do nothing
    }

    override fun onStart() {
        super.onStart()
        presenter.bind(this)
    }

    override fun onDestroy() {
        super.onDestroy()
        presenter.onDestroy()
    }
}