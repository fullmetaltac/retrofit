package ua.com.satan.retrofit.presenter


abstract class BasePresenterImpl<V> : BasePresenter<V> where  V : BaseView {

    var view: V? = null

    override fun bind(view: BaseView) {
        this.view = view as V
    }

    override fun unBind() {
        this.view = null
    }

    override fun onDestroy() {
    }
}