package ua.com.satan.retrofit

import ua.com.satan.retrofit.presenter.BasePresenter

interface MainPresenter : BasePresenter<MainView> {
    fun loadData()
}