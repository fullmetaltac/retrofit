package ua.com.satan.retrofit

import ua.com.satan.retrofit.models.RecyclerItem
import ua.com.satan.retrofit.models.movie.Movie
import ua.com.satan.retrofit.presenter.BaseView

interface MainView : BaseView {
    fun showData(results: List<RecyclerItem>)
}