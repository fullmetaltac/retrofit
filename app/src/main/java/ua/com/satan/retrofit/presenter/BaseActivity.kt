package ua.com.satan.retrofit.presenter

import android.os.Bundle
import android.support.v7.app.AppCompatActivity

abstract class BaseActivity<in V : BaseView, T : BasePresenter<V>> : AppCompatActivity(), BaseView {

    lateinit var presenter: T

    abstract fun providePresenter(): T

    abstract fun provideLayout(): Int

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(provideLayout())
        presenter = providePresenter()
        initViews()
    }

    open fun initViews() {
        // do nothing
    }

    override fun onStart() {
        super.onStart()
        presenter.bind(this)
    }

    override fun onDestroy() {
        super.onDestroy()
        presenter.onDestroy()
    }
}