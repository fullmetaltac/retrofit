package ua.com.satan.retrofit.models.feed

import com.google.gson.annotations.SerializedName

data class Feed(
        @SerializedName("page") val page: Int,
        @SerializedName("total_results") val totalResults: Int,
        @SerializedName("total_pages") val totalPages: Int,
        @SerializedName("results") val results: List<Result>
)