package ua.com.satan.retrofit

import android.support.v7.widget.LinearLayoutManager
import kotlinx.android.synthetic.main.activity_main.*
import ua.com.satan.retrofit.models.RecyclerItem
import ua.com.satan.retrofit.models.movie.Movie
import ua.com.satan.retrofit.presenter.BaseActivity
import ua.com.satan.retrofit.recycler.RecyclerAdapter
import ua.com.satan.retrofit.recycler.ScrollListener


class MainActivity : BaseActivity<MainView, MainPresenter>(), MainView {
    private val recyclerAdapter: RecyclerAdapter by lazy { RecyclerAdapter() }

    override fun provideLayout(): Int = R.layout.activity_main
    override fun providePresenter(): MainPresenter = MainPresenterImpl()
    override fun showData(items: List<RecyclerItem>) = recyclerAdapter.addData(items)

    override fun initViews() {
        super.initViews()
        rv.adapter = recyclerAdapter
        rv.layoutManager = LinearLayoutManager(this)
        rv.addOnScrollListener(ScrollListener { presenter.loadData() })

        fab.setOnClickListener { presenter.loadData() }
    }
}