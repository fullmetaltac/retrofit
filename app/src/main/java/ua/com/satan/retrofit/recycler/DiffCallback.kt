package ua.com.satan.retrofit.recycler

import android.support.v7.util.DiffUtil
import ua.com.satan.retrofit.models.Id


class DiffCallback<T>(var oldList: MutableList<T>, var newList: MutableList<T>) : DiffUtil.Callback() where T : Id {

    override fun getOldListSize() = oldList.size

    override fun getNewListSize() = newList.size

    override fun areItemsTheSame(oldItemPosition: Int, newItemPosition: Int) =
            oldList[oldItemPosition].id == newList[newItemPosition].id

    override fun areContentsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
        return oldList[oldItemPosition] == newList[newItemPosition]
    }
}