package ua.com.satan.retrofit.presenter


interface BasePresenter<in V : BaseView> {

    fun bind(view: BaseView)

    fun unBind()

    fun onDestroy()
}
