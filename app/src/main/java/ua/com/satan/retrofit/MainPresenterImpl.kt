package ua.com.satan.retrofit

import kotlinx.coroutines.experimental.android.UI
import kotlinx.coroutines.experimental.launch
import ru.gildor.coroutines.retrofit.await
import ua.com.satan.retrofit.models.RecyclerItem
import ua.com.satan.retrofit.presenter.BasePresenterImpl
import ua.com.satan.retrofit.service.ApiService
import ua.com.satan.retrofit.service.RetrofitManager

class MainPresenterImpl : BasePresenterImpl<MainView>(), MainPresenter {

    private var page: Int = 1
    private val service: ApiService by lazy { RetrofitManager.apiService }

    override fun loadData() {
        launch(UI) {
            val results = service.discover(page++).await().results
            val recyclerItems = results.map { RecyclerItem(it.id, it, service.movie(it.id).await()) }
            view?.showData(recyclerItems)
        }
    }
}