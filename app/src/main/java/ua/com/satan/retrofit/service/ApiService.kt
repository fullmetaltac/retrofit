package ua.com.satan.retrofit.service

import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query
import ua.com.satan.retrofit.BuildConfig
import ua.com.satan.retrofit.models.feed.Feed
import ua.com.satan.retrofit.models.movie.Movie


interface ApiService {
    @GET("discover/movie")
    fun discover(@Query("page") page: Int = 1, @Query("api_key") apiKey: String = BuildConfig.apiKey): Call<Feed>

    @GET("movie/{id}")
    fun movie(@Path(value = "id") id: Int, @Query("api_key") apiKey: String = BuildConfig.apiKey): Call<Movie>
}