package ua.com.satan.retrofit.recycler

import android.support.v7.util.DiffUtil
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import ua.com.satan.retrofit.R
import ua.com.satan.retrofit.models.RecyclerItem

class RecyclerAdapter : RecyclerView.Adapter<RecyclerView.ViewHolder>() {
    private var items: MutableList<RecyclerItem> = arrayListOf()

    companion object {
        const val TYPE_RESULT = 0
        const val TYPE_MOVIE = 1
    }

    fun addData(items: List<RecyclerItem>) {
        this.items.addAll(items)
        val callback = DiffCallback(this.items, items.toMutableList())
        DiffUtil.calculateDiff(callback).dispatchUpdatesTo(this)
        notifyDataSetChanged()
    }

    override fun getItemViewType(position: Int): Int =
            when (position % 2) {
                0 -> TYPE_RESULT
                else -> TYPE_MOVIE
            }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return when (viewType) {
            TYPE_RESULT -> ResultHolder(LayoutInflater.from(parent.context).inflate(R.layout.result_list_item, parent, false))
            TYPE_MOVIE -> MovieHolder(LayoutInflater.from(parent.context).inflate(R.layout.movie_list_item, parent, false))
            else -> throw Exception("No such view type!")
        }
    }

    override fun getItemCount(): Int = items.count()

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        when (holder.itemViewType) {
            TYPE_RESULT -> (holder as ResultHolder).result.text = items[position].result.title
            TYPE_MOVIE -> (holder as MovieHolder).movie.text = items[position].movie.releaseDate
        }
    }
}

class ResultHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
    val result = itemView.findViewById<TextView>(R.id.hit_item)
}

class MovieHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
    val movie = itemView.findViewById<TextView>(R.id.user_item)
}