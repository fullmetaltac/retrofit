package ua.com.satan.retrofit.models

import ua.com.satan.retrofit.models.feed.Result
import ua.com.satan.retrofit.models.movie.Movie

data class RecyclerItem(
        override val id: Int,
        val result: Result,
        val movie: Movie
) : Id