package ua.com.satan.retrofit.models.movie

import com.google.gson.annotations.SerializedName

data class SpokenLanguages(
        @SerializedName("iso_639_1") val iso6391: String,
        @SerializedName("name") val name: String
)